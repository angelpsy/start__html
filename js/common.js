// скроллинг по клику по якорям в меню
(function() {
    $('a[href^="#section"]').bind('click.smoothscroll', function(e) {
        e.preventDefault();

        var target = this.hash,
            $target = $(target);

        $('html, body').stop().animate({
            'scrollTop': ($target.offset().top - 100) //после .top можно задать изменение положения конечной позиции
        }, 500, 'swing', function() {});
        return false;
    });
})();

// Фиксированное меню после прокрутки
(function() {
    $(".page__header").addClass("absolute");

    $(window).scroll(function() {
        if ($(this).scrollTop() > 230) {
            $(".page__header.absolute").addClass("fixed").removeClass("absolute");
            $(".page__header.fixed").fadeIn(500);
        } else {
            $(".page__header.fixed").addClass("absolute").removeClass("fixed");
        }
    });
})();

// Дублируем информацию из одного поля с именем/телефонов/email во все подобные поля
(function () {
	$("[class*=field--]").change(function(event) {
		var el = $(event.target),
			value = el.val(),
			classEl = el.attr("class"),
			fieldClassEl = classEl.match(/field--[a-z]*/)[0];
		$("." + fieldClassEl).val(value);
	});
})();

//Обработка форм
(function () {
	function openForm () {
	$(".wrapper--form").fadeIn(500);

	if (($(document).width()) < 1000) { /* для маленьких экранов смотрим положение скрола и туда выводим форму*/
		var scrollTop = $("html").scrollTop() || $("body").scrollTop();
		$(".wrapper--form").height($(document).height()).css({"position": "absolute"});
		$(".wrapper--form form").css({"margin-top":scrollTop});
	};
	}

	$(".open-form").click(function () {
		openForm ();
		return false;
	});

	$(".close-form").click(function () {
		$(this).parents(".wrapper--form").fadeOut(500);
	});

	$(".wrapper--form").on("click", function (event) {
        var clickTarget = event.target.className;
        if (clickTarget != "wrapper--form") {return};
        $(".wrapper--form").fadeOut(500);
        return false;
    });

})();
